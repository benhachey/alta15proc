#!/usr/bin/perl -w

#Makes an HTML index page and bib files

use strict;

use vars qw (@Pos $i $h @F $a $b $t $au $fm);

#Create bib for book

#the size of the front matter in pages
$fm =10;

open(O,">bib/U14-1000.bib")||die;
print O "\@Book\{ALTA2014\:2014,\n";
print O  "editor    = {Gabriela Ferraro and Stephen Wan},\n";
print O "title \= \{Proceedings of the Australasian Language Technology Association Workshop 2014\}\,\n";
print O "month     \= \{November\}\,\n";
print O "year      \= \{2014\}\,\n";
print O "address   \= \{Melbourne\, Australia\}\n\}\n";
print O "url       = {http://www.aclweb.org/anthology/U14-1.pdf}\n";
close(O);

system("cat bib/U14-1000.bib > bib/U14-1.bib");

undef @Pos;
open(I,"Proceedings.toc_aux")||die;
$i = 1001;

while(<I>) {
    chomp;

    /\\it (.*?) ?\} \\\\ ?(.*?) ?\}\{(\d+)\}\{(\d+)\}$/;
    $t = $1; $au = $2; $a = $3; $b = $4;
    push @Pos,"$a\-$b";
    open(O,">bib/U14-$i.bib")||die;
    $_ = $au;
    s / and //g;
    s /[ \,\"\\]//g;
    print O "\@InProceedings\{$_:2014:ALTA2014\,\n";
    print O "author \= \{$au\}\,\n";
    print O "title \= \{$t\}\,\n";
    print O "booktitle \= \{Proceedings of the Australasian Language Technology Association Workshop 2014\}\,\n";
    print O "month     \= \{November\}\,\n";
    print O "year      \= \{2014\}\,\n";
    print O "address   \= \{Melbourne\, Australia\}\,\n";
    # PC 23.11.2012
    # $_ = $a + $b - 1;
    $_ = $b;
    print O "pages \= \{$a\-\-$_\}\,\n";
    my $e = $_;
    $_ = 1000 + $i;
    print O "url \= http://www.aclweb.org/anthology/U14\-$i\n";
    print O "\}\n";
    close(O);
    system("cat bib/U14-$i.bib >> bib/U14-1.bib");


    ##Print index file
    print "<tr class=\"bg1\">\n";
    print "<td valign=\"top\"><a href=\"pdf/U14-$i.pdf\">pdf<\/a><\/td>\n";
    print "<td valign=\"top\"><a href=\"bib/U14-$i.bib\">bib<\/a><\/td>\n";
    print "<td valign=\"top\"><i>$t</i><br><b>$au<\/b><\/td>\n";
    print "<td valign=\"top\"><a name=\"49\">pp.&nbsp;$a\&\#8211\;$e\<\/a><\/td>\n";
    print "<\/tr>\n";
    $i++;
}
close(I);

#copy preface
undef @F;
for($h=1;$h<=$fm;$h++) {
    $_ = sprintf("%04d", $h);
    push @F,"pdf_pages/pg\_$_\.pdf";
}
$_ = join " ",@F;
system("pdftk $_ cat output pdf/U14-1000.pdf");
#system("cat bib/* > anth.bib");
#system("cp anth.bib bib/U14-1.bib");
system("cp Proceedings.pdf anth.pdf");
system("cp Proceedings.pdf pdf/U14-1.pdf");


for($i=0;$i<=$#Pos;$i++) {
    ($a,$b) = split(/\-/,$Pos[$i]);
    $a = $a + $fm;
    # PC 23.11.2012
    # $b = $a - 1 + $b;
    $b = $b + $fm;
    my $j = 1001 + $i;
    if ($a == $b) {
	$a = sprintf("%04d", $a);
	system("cp pdf_pages/pg_$a.pdf pdf/U14-$j.pdf");
    }
    else {
	undef @F;
	for($h=$a;$h<=$b;$h++) {
	    $_ = sprintf("%04d", $h);
	    push @F,"pdf_pages/pg\_$_\.pdf";
	}
	$_ = join " ",@F;

	system("pdftk $_ cat output pdf/U14-$j.pdf");
    }
}



