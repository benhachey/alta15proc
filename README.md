# Create ALTA 2015 proceedings

Adapted from [https://www.dropbox.com/sh/l7bogmp11yvqpch/AABQuWPBHiY7U1kpNH08C2MYa?dl=0](https://www.dropbox.com/sh/l7bogmp11yvqpch/AABQuWPBHiY7U1kpNH08C2MYa?dl=0).

NOTE: It would probably be quicker to skip steps 1-6 and create papers.txt and order.txt files manually.

## STEP 1 

Fetch easy2acl from [https://github.com/nblomqvist/easy2acl](https://github.com/nblomqvist/easy2acl):
```
git clone git@github.com:nblomqvist/easy2acl.git
cd easy2acl
```

## STEP 2

Copy submissions and acceptance info from EasyChair as described at [https://github.com/nblomqvist/easy2acl#getting-data-from-easychair](https://github.com/nblomqvist/easy2acl#getting-data-from-easychair).

## STEP 3

Generate db and final pdf data using easy2acl script:
```
virtualenv -p python3 ve
. ve/bin/activate
pip install PyPDF2 unicode-tex
python easy2acl/easy2acl.py
```

NOTE: It may be necessary to create a db entry and final pdf manually for papers that the script doesn't handle.

## STEP 4

Check data quality following directions at [https://github.com/nblomqvist/easy2acl#additional-information](https://github.com/nblomqvist/easy2acl#additional-information). Ensure only accepted papers are listed in db and with pdf files in final.

## STEP 5

Add data for shared task to db file. Add pdf files to final directory.

## STEP 6

Convert db file to format expected by proc.pl:
```
python scripts/convert.py
```

## STEP 7

Manually split papers.txt into longpapers.txt and shortpapers.txt. Manually split order.txt into longorder.txt and shortorder.txt. Manually create sharedtaskpapers.txt and sharedtaskorder.txt.

## STEP 8

Create paper contents:
```
mkdir tex
perl scripts/proc.pl longpapers.txt longorder.txt > tex/longpapers.tex
perl scripts/proc.pl shortpapers.txt shortorder.txt > tex/shortpapers.tex
perl scripts/proc.pl sharedtaskpapers.txt sharedtaskorder.txt > tex/sharedtaskpapers.tex
```

NOTE: The year is hard-coded in this script and should be updated before running.

## STEP 9

Edit Proceedings.tex file to include correct publication data, sponsor data, programme committee, preface, programme, long/short/sharedtask sections.

```
pdflatex Proceedings.tex
pdflatex Proceedings.tex
pdflatex Proceedings.tex
```

NOTE: Editing will take several hours.

NOTE: It may be necessary to do some embedded fonts magic as in 2014. If so, document here.

## STEP 10

Create directory structure for anthology:
```
mkdir -p anth/ALTA/bib anth/ALTA/pdf U15
```

## STEP 11

Build table of contents:
```
python scripts/toc_to_toc_aux.py > Proceedings.toc_aux
```

NOTE: The year and number of front matter pages are hard-coded in this script and should be updated before running.

NOTE: It may be necessary to do some ascii transliteration as in 2014. If so, document here.

## STEP 12

Build anthology contents:
```
perl scripts/proc2.pl
perl scripts/proc3.pl > index_ALTA2015.html
```

NOTE: The year, editors, month, address and number of front matter pages are hard-coded in both scripts and should be updated before running.

## STEP 13

Package anthology contents:
```
zip -r U15.zip U15/
zip -r copyright.zip copyright/
```

Send anthology package and completed copyright forms to Min Yan Kan <kanmy@comp.nus.edu.sg>, or current ACL Anthology maintainer.